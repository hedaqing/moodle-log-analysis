# moodle log DDL

---

### log table before moodle 2.4
```
-- auto-generated definition
create table moodle_log.mdl_log
(
	id bigint(10) default '0' not null,
	time bigint(10) default '0' not null,
	userid bigint(10) default '0' not null,
	ip varchar(45) default '' not null,
	course bigint(10) default '0' not null,
	module varchar(20) default '' not null,
	cmid bigint(10) default '0' not null,
	action varchar(40) default '' not null,
	url varchar(100) default '' not null,
	info varchar(255) default '' not null
)
;
```

### log table after moodle 2.4

```
-- auto-generated definition
create table moodle_log.mdl_logstore_standard_log
(
	id bigint(10) default '0' not null,
	eventname varchar(255) default '' not null,
	component varchar(100) default '' not null,
	action varchar(100) default '' not null,
	target varchar(100) default '' not null,
	objecttable varchar(50) null,
	objectid bigint(10) null,
	crud varchar(1) default '' not null,
	edulevel tinyint(1) not null,
	contextid bigint(10) not null,
	contextlevel bigint(10) not null,
	contextinstanceid bigint(10) not null,
	userid bigint(10) not null,
	courseid bigint(10) null,
	relateduserid bigint(10) null,
	anonymous tinyint(1) default '0' not null,
	other longtext null,
	timecreated bigint(10) not null,
	origin varchar(10) null,
	ip varchar(45) null,
	realuserid bigint(10) null
)
;
```